<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Hege Refsnes">
    <title>jQuery Plus PHP Form Handling</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <div id="mydata">
                    <ul></ul>
                </div>
                <button id="show-data" cclass="btn btn-default">Show Data</button>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.2.1.min"></script>
    <script src="js/getdata.js"></script>
</body>

</html>