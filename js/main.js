$(document).ready(function() {

    //creating empty variables for each fields to track
    //if any field is empty then there is an error
    var name = '';
    var email = '';
    var gender = '';
    var foods = '';
    var education = '';
    var languagelist = '';

    //validating name
    $("#uname").keyup(function() {
        var vall = $(this).val();

        if (vall == '') {
            $("#nameError").addClass('has-error');
            $("#nameError .help-block").html("");
            name = '';
        } else if (vall.length < 3) {
            $("#nameError").addClass('has-error');
            $("#nameError .help-block").html("At least 3 Characters Required");
            name = '';

        } else {
            $("#nameError").removeClass('has-error');
            $("#nameError").addClass('has-success');
            $("#nameError .help-block").html("Voila");
            name = vall;
            console.log(name);
        }
    });

    //validating email
    $("#uemail").keyup(function() {
        var vall = $(this).val();

        if (vall == '') {
            $("#emailError").addClass('has-error');
            email = '';
        }
        else 
        {
            $.ajax({
                url: '/ultimateform/emailchecker.php',
                type: 'post',
                //data send either string or array or object format
                //carefull about space while you are using string 'email =' not equal to 'email='
                data: 'email='+vall,
                success:function(data){
                    if (data == 'invalid') {
                        $("#emailError").addClass('has-error');
                        $("#emailError .help-block").html("Email format is invalid");
                        email = '';
                    }
                    else if(data == 'exsits')
                    {
                        $("#emailError").addClass('has-error');
                        $("#emailError .help-block").html("Email already exsits");
                        email = '';
                    }
                    else if(data == 'ok')
                    {
                        $("#emailError .help-block").html("You are good to go..");
                        $("#emailError").removeClass('has-error');
                        $("#emailError").addClass('has-success');
                        email = vall;
                        console.log(vall);
                    }
                }
            })           
        }
    });

    //Gender validation 
    $("#male").click(function() {
        var vall = $(this).val();
        gender = vall;
        console.log(gender);
    });
    $("#female").click(function() {
        var vall = $(this).val();
        gender = vall;
        console.log(gender);
    });

    //Food validation
     $("#foodlist :checkbox").click(function() {
        var foodslist = [];
        $("#foodlist :checkbox:checked").each(function() {
            foodslist.push($(this).val());
        });
        var checkedFood;
        checkedFood = foodslist.join(',');
        if ((checkedFood.length == 0)) {
            alert("Check At least One Item");
            $("#foodsError .text-danger").html("At least one food required");
            foods = '';
        }
       else{
            foods = checkedFood;
            $("#foodsError .text-danger").html("");
            console.log(foods);
            //console.log(foods.length);
       }
    });

    //level of education
    $("select#educationLabel").change(function() {
        var vall = $(this).val();
        if (vall == '') 
        {
            alert('At least  1  Select');
            $("#educationError").addClass('has-error');
            $("#educationError .text-danger").html("Education level is Required");
            education = '';
        }
        else
        {   
            $("#educationError").removeClass('has-error');
            $("#educationError").addClass('has-success');
            $("#educationError .text-danger").html("");            
            education = vall;
            console.log(education);
        }
    });        

    //programming language list
    $("select#langlist option").click(function() {
        var languages = [];
        $("select#langlist option:selected").each(function() {
            languages.push($(this).val());
        });
        var proglanguages;
        proglanguages = languages.join(',');
        if ((proglanguages.length == 0)) {
            alert("Check At least One Item");
            $("#languagelistError").addClass('has-error');
            $("#languagelistError .text-danger").html("At least one Programming language required");
            languagelist = '';
        }
       else{
            languagelist = proglanguages;
            $("#languagelistError").removeClass('has-error');
            $("#languagelistError").addClass('has-success');  
            $("#languagelistError .text-danger").html("");
            console.log(languagelist);
            //console.log(proglanguages.length);
       }
    });

    //submitting the form after collecting the values
    $("#submit_form").click(function(event) {
      event.preventDefault();
      if (name == '' || email == '' || gender == '' || foods == '' || education == '' || languagelist == '') {
        alert('correct the errors');
      }
      else{
        $.ajax({
            url: '/ultimateform/process.php',
            type: 'post',
            data:'name='+name+'&email='+email+'&gender='+gender+'&foods='+foods+'&education='+education+'&languagelist='+languagelist,
            success:function(data){
                if (data == 'done') {
                    alert('Your are signed up dude');
                }
                else{
                    alert('Bad luck dude,try again');
                }
            }
        })        
      }
    });

});
