-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2017 at 07:33 PM
-- Server version: 5.7.9
-- PHP Version: 5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `final_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `ultimate_crud`
--

DROP TABLE IF EXISTS `ultimate_crud`;
CREATE TABLE IF NOT EXISTS `ultimate_crud` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `foods` varchar(60) NOT NULL,
  `education` varchar(60) NOT NULL,
  `language_list` varchar(60) NOT NULL,
  `join_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ultimate_crud`
--

INSERT INTO `ultimate_crud` (`id`, `name`, `email`, `gender`, `foods`, `education`, `language_list`, `join_time`) VALUES
(29, 'Subbir Hosaine', 'subbir.ict@gmail.com', 'Male', 'Steak', '3', '2,3', '2017-06-23 17:47:12'),
(28, 'testmia', 'subbeir.ict@gmail.com', 'Male', 'Steak,Pizza', '3', '1,2', '2017-06-23 17:46:17'),
(27, 'FTNS Department', 'subbdir.ict@gmail.com', 'Male', 'Steak', '3', '3,4', '2017-06-23 17:26:26'),
(26, 'Subbir eHosain', 'subbier.ict@gmail.com', 'Male', 'Steak', '2', '3', '2017-06-23 17:12:35'),
(25, 'Subbir eHosain', 'subbier.ict@gmail.com', 'Male', 'Steak', '2', '3', '2017-06-23 17:12:24');

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `un` varchar(30) NOT NULL,
  `fn` varchar(30) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `level` enum('admin','faculty') NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `un`, `fn`, `pwd`, `level`) VALUES
(1, 'subir', 'subbir hosain', 'sfdasdfasf', 'admin');  







/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
