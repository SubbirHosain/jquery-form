<?php 

    require_once ( 'install/connect_db.php' );

    //connnect db
    //include 'db/db.php';
    //define error variables set either null or empty values
    $nameError = '';
    $emailError = '';
    $genderError = '';
    $foodsError = '';
    $educationError = '';
    $languagelistError = '';
    
    //To hold the form value first initialize them to empty
    $name = '';
    $email = '';
    $gender = '';
    $foods = '';
    $education = '';
    $languagelist ='';

    //sanitize all form fields data
    function validate_input($data){
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    //imagine everything is valid means there is no error
    $valid = true;  

    //Checking if the form is submitted or not    
    if(isset($_POST['form_submit']))
    {
        //checking name field
        if (empty($_POST['name'])) 
        {
            $nameError = "Please enter Name";
            $valid = false;
        }
        else
        {
            $name = validate_input($_POST['name']);
            if(strlen($name)<3)
            {
                $nameError = "The name must be at least 3 characters longer";
                $valid = false;
            }
            else if (!preg_match("/^[a-zA-Z ]*$/", $name))
            {
                $nameError = "Only letters and white space allowed";
                $valid = false;
            }
           
        }

        //checking email
        if (empty($_POST['email'])) 
        {
            $emailError = "Please enter Email";
            $valid = false;
        }   
        else
        {
            $email = validate_input($_POST['email']);
            //check if e-mail address is well-formed
            if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            {
                $email = "Invalid email format";
                $valid = false;
            }
            
        }

        //checking gender 
        if (empty($_POST['gender'])) 
        {
            $genderError = "Your Gender is Required";
            $valid = false;
        }
        else
        {
            $gender = validate_input($_POST['gender']);
            
        }

        //Favourite Foods
        if (empty($_POST['foods'])) 
        {
            $foodsError = "At least one food required";
            $valid = false;
        }
        else
        {
            $foodlist = $_POST['foods'];
                   
            $foods = implode(',', $foodlist);
            

        }
        //checking School 
        if (empty($_POST['education'])) 
        {
            $educationError = "Education level is Required ";
            $valid = false;
        }
        else
        {
            $education = validate_input($_POST['education']);
            
        }

        //Language list
        if (empty($_POST['languagelist'])) 
        {
            $languagelistError = "At least one Programming language required";
            $valid = false;
        }
        else
        {
            $planguagelist = $_POST['languagelist'];
                            
            $languagelist = implode(',', $planguagelist);
            

        }

        //if everything is ok 
        if ($valid) {
            $sql = "INSERT INTO ultimate_crud (name,email,gender,foods,education,language_list) VALUES (?,?,?,?,?,?)";
            $sth = $dbh->prepare($sql);
            $data = array($name,$email,$gender,$foods,$education,$languagelist,);
            $sth->execute($data);

            echo "<h1>everything sucess</h1>";

            //to clear the form values after the successful submission
            unset($_POST); 

            //exit();
            
            //Redirect to resubmit via reloading page but success message does not exist
            //header('location:'.$_SERVER['PHP_SELF']);     
        }       
        
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Free Web tutorials">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Hege Refsnes">
    <title>jQuery Plus PHP Form Handling</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Server-Side Processing</a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="#">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container section-padding">
        <div class="row">
            <div class="col-md-offset-3 col-sm-6">
                <form action="" method="POST">
                    <!-- Name -->
                    <div class="form-group <?php echo !empty($nameError)?'has-error':'';?>" id="nameError" >
                        <label for="fname">Name</label>
                        <input type="text" name="name" id="uname" value="<?php echo isset($_POST['name']) ? $_POST['name'] : '' ; ?>" class="form-control" placeholder="Name">
                        <span class="help-block"><?php echo !empty($nameError)?$nameError:'' ;?></span>
                    </div>
                    <!-- Password -->
                    <div class="form-group <?php echo !empty($emailError)?'has-error':'';?>" id="emailError">
                        <label for="email">Email</label>
                        <input type="text" name="email" value="<?php echo isset($_POST['email']) ? $_POST['email'] : '' ; ?>" id="uemail" class="form-control" placeholder="Email">
                        <span class="help-block"><?php echo !empty($emailError)?$emailError:'' ;?></span>
                    </div>                    
                    <!-- Gender -->
                    <div class="form-group">
                        <p>Gender:</p>
                        <label class="radio-inline">
                            <input type="radio"  <?php if (isset($_POST['gender']) && $_POST['gender']== "Male") {
                                echo "checked="."\"checked\"";
                            }  ?> name="gender" id="male" value="Male"> Male
                        </label>
                        <label class="radio-inline">
                            <input type="radio" <?php if (isset($_POST['gender']) && $_POST['gender']=="Female") {
                                echo "checked="."\"checked\"";
                            }  ?> name="gender"  id="female" value="Female"> Female
                        </label>
                        <p class="text-danger"><?php echo !empty($genderError)?$genderError:'' ;?></p>
                    </div>                    
                    <!-- choose your favourite food using checkboxes -->
                    <div class="form-group" id="foodsError">
                        <p>Please choose your favorite foods:</p>
                        <div class="food-section" id="foodlist">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="foods[]" <?php if (isset($_POST['foods'])) {
                                    foreach ($_POST['foods'] as $food_name) {
                                        if ($food_name == 'Steak') {
                                            echo "checked=\"checked\"";
                                            break;
                                        }
                                    }
                                } ?> value="Steak"> Steak
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" <?php if (isset($_POST['foods'])) {
                                    foreach ($_POST['foods'] as $food_name) {
                                        if ($food_name=='Pizza') {
                                            echo "checked=\"checked\"";
                                            break;
                                        }
                                    }
                                } ?> name="foods[]"  value="Pizza"> Pizza
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="foods[]" <?php if (isset($_POST['foods'])) {
                                    foreach ($_POST['foods'] as $food_name) {
                                        if ($food_name=='Chicken') {
                                            echo "checked=\"checked\"";
                                            break;
                                        }
                                    }
                                } ?> value="Chicken"> Chicken
                            </label>
                            <p class="text-danger"><?php echo !empty($foodsError)?$foodsError:'' ;?></p>
                        </div>
                    </div>

                    <!-- School selection -->
                    <div class="form-group <?php echo !empty($educationError)?'has-error':'';?>" id="educationError">
                        <p>Select a Level of Education:</p>
                        <select name="education" id="educationLabel" class="form-control">
                            <option value=''>Select Your School</option>

                            <option <?php if (isset($_POST['education']) && $_POST['education']==1) {
                                echo "selected="."\"selected\"";
                            } ?> value="1">High School</option>

                            <option <?php if (isset($_POST['education']) && $_POST['education']==2) {
                                echo "selected="."\"selected\"";
                            } ?> value="2">College</option>

                            <option <?php if (isset($_POST['education']) && $_POST['education']==3) {
                                echo "selected="."\"selected\"";
                            } ?> value="3">University</option>
                        </select>   
                        <p class="text-danger"><?php echo !empty($educationError)?$educationError:'' ;?></p>
                    </div>

                    <!-- Programming List-->
                    <div class="form-group <?php echo !empty($languagelistError)?'has-error':'';?>"" id="languagelistError">
                        <p>Select Programming Language:</p>
                    <div id="languagelistprog">
                        <select name="languagelist[]" multiple size="4" id="langlist" class="form-control">
                            <option <?php if (isset($_POST['languagelist'])) {
                                foreach ($_POST['languagelist'] as $language) {
                                    if ($language==1) {
                                        echo "selected="."\"selected\"";
                                    }
                                }
                            } ?> value="1">C</option>

                            <option <?php if (isset($_POST['languagelist'])) {
                                foreach ($_POST['languagelist'] as $language) {
                                    if ($language==2) {
                                        echo "selected="."\"selected\"";
                                    }
                                }
                            } ?> value="2">C++</option>    

                            <option <?php if (isset($_POST['languagelist'])) {
                                foreach ($_POST['languagelist'] as $language) {
                                    if ($language==3) {
                                        echo "selected="."\"selected\"";
                                    }
                                }
                            } ?> value="3">Java</option>  

                            <option <?php if (isset($_POST['languagelist'])) {
                                foreach ($_POST['languagelist'] as $language) {
                                    if ($language==1) {
                                        echo "selected="."\"selected\"";
                                    }
                                }
                            } ?> value="4">PHP</option>  
                        </select>
                        <p class="text-danger"><?php echo !empty($languagelistError)?$languagelistError:'' ;?></p>
                    </div>
                    </div>
                    <button type="submit" name="form_submit" id="submit_form" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <script src="js/jquery-3.2.1.min"></script>
    <script src="js/main.js"></script>
</body>

</html>