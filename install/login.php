<!DOCTYPE HTML>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <script src="js/jquery.min.js"></script>
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <title>QuickSlots | Login</title>
    <script src="js/form.js"></script>
</head>

<body class="center">
    <div class="vspacer"></div>
    <div class="box middle">
        <div class="boxbg"></div>
        <div class="elements">
            <div class="avatar">
                <div class="icon key"></div>
            </div>
            <div class="title">Login</div>
            <form id="loginform" method="post" action="login.php">
                <input type="text" name="uName" class="styled username" required placeholder="Username/Roll No." />
                <input type="password" name="pswd" class="styled pswd" required placeholder="Password" />
                <div class="blocktext info"></div>
                <div class="center button">
                    <button>Login</button>
                    <div class="loader">
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="footer" style="margin:0">Powered by <a href="#">Subbir</a></div>
</body>

</html>
