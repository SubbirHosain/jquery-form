<?php 
    /**
     * Provides interface for initial system setup and admin account registration.
     * 
     */

    //inside config.php there is a config array to hold database details
    if (file_exists('config.php')){
      include('config.php');
    }

    //assign form database info into $config if config.php doesn't exist
    if (empty($config)) {

        $config = $_POST;
    }
    //on success establish db connection and write config file
    $valid = true;
    if ($config) 
    {   
        //validate empty fields
        if(empty($config['db_host']))
        {
            $db_hostError = "<p style='color:red'>Database host name is required</p>";
            $valid = false;
        }
        if(empty($config['db_name']))
        {   
            $db_nameError = "<p style='color:red'>Database name  is required</p>";
            $valid = false;
        }
        if(empty($config['db_user']))
        {   
            $db_userError = "Database user  is required";
            $valid = false;
        } 
        if(empty($config['db_pswd']))
        {   
            $db_passError = "Database password  is required";
            $valid = false;
        }   

        //if everything is ok  
        if($valid)
        {
            // Establish mysql connection
           try {
                $dsn = "mysql:host={$config['db_host']};dbname={$config['db_name']}";
                $options = array(PDO::ATTR_PERSISTENT => true);

                $dbh = new PDO($dsn, $config['db_user'], $config['db_pswd'], $options);
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);   

                $conf = "<?php\n  \$config = " . var_export($config,true) . ";\n?>";

                //write  the database configuration to a file which will be used later by connect_db.php
                file_put_contents('config.php',$conf);

                //Check if the system has no admin configured yet
                $query = $dbh->query("SELECT count(*) FROM user where level='admin'");
                //if the the table user not found then an excetpion will be thrown 
                //this is our tricky part to create or import tables from sql dump file
                //SQLSTATE[42S02]: Base table or view not found: 1146 Table 'learnphp.user' doesn't exis
                //catch it in pdoexcetpion and create the tables
                
                // If the system already has an admin configured, we redirect to login page instead.
                $result = $query->fetch()[0];
                if ($result) {
                    
                    header("location:login.php");

                }
                

            } catch (PDOException $e) {

                //SQLSTATE[42S02]: Base table or view not found: 1146 Table 'learnphp.user' doesn't exist
                if ($e->getCode() == '42S02') {
                    //create the required tables from sql dump file and again redirect
                    $dbh->exec(file_get_contents('sql/ultimate_crud.sql'));
                    //when the request is coming from setup.php and $_post exists 
                    if ($_POST)
                    {
                        //after sucessfull database connection and tables redirect for setup.php page either login or register
                        header("location:setup.php");
                    }
                }
                //when the request is coming from setup.php 
                //else manullay corrupt config.php
                if($_POST){
                    if ($e->getCode() == 1045) {
                        echo "Cannot connect to the database: Invalid username or password: ";
                    }
                    else if($e->getCode()==2002){
                        echo "Invalid host";
                    }
                    else{
                        echo 'Error is: '.$e->getMessage();
                        
                    }
                }
                else{
                    //if manually config.php is provided and its wrong then does it
                    //copy the invalid config.php and make it empty to start the setup again
                    copy('config.php','tmp/config.old_invalid.php');
                    file_put_contents('config.php','');
                }
            } 
        }          
    }

?>
<!DOCTYPE HTML>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" type="image/png" href="images/favicon.png" />
    <link href="../css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../css/chosen.css" rel="stylesheet" type="text/css" />

    <title>QuickSlots | Setup</title>
</head>

<body class="center">
    <div class="vspacer"></div>
    <div class="box" style="vertical-align: middle">
        <div class="boxbg"></div>

        <!-- check db connection here -->
        <?php if(empty($dbh)):?>
            
        <div class="db"></div>
        <div class="title">Setup a databse for quickslots to use</div>
        <div class="elements">
            <!-- form for database installation -->
            <form method="POST" id="setup-form" action="setup.php">
                <?php echo isset($db_hostError) ? $db_hostError : '' ; ?>
                <input type="text" name="db_host" class="styled details"  title="Please enter database host" placeholder="Database host" />
                
                <?php echo isset($db_nameError) ? $db_nameError : '' ; ?>
                <input type="text" name="db_name" class="styled details"  title="Please enter database name" placeholder="Database Name" />
                
                <?php echo isset($db_userError) ? $db_userError : '' ; ?>
                <input type="text" name="db_user" class="styled username"  title="Please enter database username" placeholder="Database User" />
                
                <?php echo isset($db_passError) ? $db_passError : '' ; ?>
                <input type="password" name="db_pswd" class="styled pswd" title="Please enter database password" placeholder="Database Password" />

                <div class="blocktext info"></div>
                <div class="center button">
                    <button type="submit" name="db_setup">Continue</button>
                </div>
            </form>
        </div>

        <!-- if db connection is all setup then register for site admin account -->
        <?php else:?>

        <div class="avatar">
            <div class="add icon"></div>
        </div>
        <div class="title">To start using the system,
            <br /> create an admin/dean account...</div>

        <div class="elements">
            <form method="post" action="register.php?action=addUser">

                <input type="text" name="uName" class="styled username" required pattern="[^ ]{3,25}" title="3 to 25 characters without spaces" placeholder="Username" />

                <input type="text" name="fullName" class="styled uInfo" required pattern=".{6,50}" title="6 to 50 characters" placeholder="Full Name" />

                <input type="password" name="pswd" class="styled pswd" required pattern="[^ ]{8,32}" title="8 to 32 characters without spaces" placeholder="Password" />

                <input type="password" class="styled pswd" required pattern="[^ ]{8,32}" title="8 to 32 characters without spaces" placeholder="Confirm password" />

                <div class="blocktext info"></div>
                <div class="center button">
                    <button>Register</button>
                </div>
            </form>
        </div>
        <?php endif;?>
    </div>
</body>

</html>