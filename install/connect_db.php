<?php

/**
 * Initiates database connections
 * Required by all files that needs to perform db operation
 */

//if config.php doesn't exist then create config.php using setup.php
//in the config.php there is stored db details
//check the file including otherwise when this file will be included to another file it will get error
$filename = include 'config.php';
if (file_exists($filename)) {
  include ('config.php');

}

//database connection if valid config.php exists
if(isset($config))
{
  try
  {
    // Establish mysql connection
    $dsn = "mysql:host={$config['db_host']};dbname={$config['db_name']}";
    $options = array(PDO::ATTR_PERSISTENT => true);

    $dbh = new PDO($dsn, $config['db_user'], $config['db_pswd'], $options);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }
  catch(PDOException $e){
    //SQLSTATE[HY000] [1049] Unknown database 'mdmustaf_learnphp'
    //if database details not found
    //manually database name corrupt
    if ($e->getCode() == 1049) {
      copy('config.php','tmp/config.old_invalid.php');
      file_put_contents('config.php','');
      header("Location: ./setup.php");
    }
    //when manually file is corrupt
    //SQLSTATE[HY000] [2002] php_network_getaddresses: getaddrinfo failed: No such host is known.
    else if($e->getCode() == 2002){
      copy('config.php','tmp/config.old_invalid.php');
      file_put_contents('config.php','');
      header("Location: ./setup.php");
    }
    die($e->getMessage()."\n");
  }
}
else
{
  //loading the database installation form if database connection doesn't exist
  echo 'sdfsf';
  die();
}
?>