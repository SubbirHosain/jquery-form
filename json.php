<?php 

require_once 'connect_db.php';

    $sql = "SELECT * FROM ultimate_crud";
    $sth = $dbh->prepare($sql);
    $sth->execute();
    $data_array = $sth->fetchAll(PDO::FETCH_ASSOC);

    echo json_encode(array('data'=>$data_array));
